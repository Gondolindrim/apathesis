\contentsline {chapter}{Abstract}{7}{Doc-Start}% 
\contentsline {chapter}{Zusammenfassung}{9}{Doc-Start}% 
\contentsline {chapter}{Resumo}{11}{Doc-Start}% 
\contentsline {chapter}{List of Figures}{13}{section*.1}% 
\contentsline {chapter}{List of Tables}{15}{section*.2}% 
\contentsline {chapter}{Abbreviations and Acronyms}{17}{section*.2}% 
\contentsline {chapter}{List of Symbols}{19}{chapter*.3}% 
\contentsline {chapter}{\chapternumberline {1}First steps: setting up your thesis}{25}{chapter.1}% 
\contentsline {section}{\numberline {1.1}How the template is organized}{25}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Changing page geometry}{27}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Changing fonts used}{27}{section.1.3}% 
\contentsline {section}{\numberline {1.4}How to build the main.tex file}{27}{section.1.4}% 
\contentsline {subsection}{\numberline {1.4.1}In a command-line}{27}{subsection.1.4.1}% 
\contentsline {subsection}{\numberline {1.4.2}In a dedicated TeX editor}{28}{subsection.1.4.2}% 
\contentsline {section}{\numberline {1.5}Setting up university logo, thesis title, front matter and abstracts}{28}{section.1.5}% 
\contentsline {subsection}{\numberline {1.5.1}Change basic metadata: title, names and university logo}{28}{subsection.1.5.1}% 
\contentsline {subsection}{\numberline {1.5.2}Second front matter}{28}{subsection.1.5.2}% 
\contentsline {subsection}{\numberline {1.5.3}Catalographic card}{28}{subsection.1.5.3}% 
\contentsline {subsection}{\numberline {1.5.4}Abstracts}{28}{subsection.1.5.4}% 
\contentsline {subsubsection}{Modifying abstract properties}{29}{lstnumber.1.4.5}% 
\contentsline {subsection}{\numberline {1.5.5}Acronyms and symbols}{29}{subsection.1.5.5}% 
\contentsline {subsection}{\numberline {1.5.6}Epigraph}{29}{subsection.1.5.6}% 
\contentsline {subsection}{\numberline {1.5.7}First compile and adding packages}{30}{subsection.1.5.7}% 
\contentsline {section}{\numberline {1.6}Adding text chapters and appendixes}{30}{section.1.6}% 
\contentsline {chapter}{\chapternumberline {2}Figures and tables}{31}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Adding figures}{31}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Figure placement}{32}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}Scaling figures}{32}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}Rotating figures}{33}{subsection.2.1.3}% 
\contentsline {section}{\numberline {2.2}Adding tables}{33}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Coding your table}{34}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Limited width caption}{35}{subsection.2.2.2}% 
\contentsline {subsection}{\numberline {2.2.3}Adding notes to your table}{36}{subsection.2.2.3}% 
\contentsline {subsection}{\numberline {2.2.4}Adjusting table row height}{36}{subsection.2.2.4}% 
\contentsline {subsection}{\numberline {2.2.5}Adjusting table column width}{37}{subsection.2.2.5}% 
\contentsline {chapter}{Bibliography}{39}{chapter*.20}% 
\contentsline {part}{Appendixes}{41}{chapter*.20}% 
\contentsline {appendix}{\chapternumberline {A}PV Panel Curves and simulation program}{43}{appendix.A}% 
\contentsline {appendix}{\chapternumberline {B}Second appendix}{53}{appendix.B}% 
